package bd.com.dod.tictactoe;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.String;
import android.view.Menu;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button[][] buttons = new Button[3][3];

    private boolean me1Turn = true;

    private int roundCount;

    private int me1Points;
    private int you2Points;

    private TextView textViewMe1;
    private TextView textViewYou2;

    AlertDialog.Builder builder;

    private int fff;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewMe1 = findViewById(R.id.text_view_p1);
        textViewYou2 = findViewById(R.id.text_view_p2);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                String buttonID = "button_" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i][j] = findViewById(resID);
                buttons[i][j].setOnClickListener(this);
            }
        }

        Button buttonReset = findViewById(R.id.button_reset);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGame();
            }
        });
    }

    @SuppressLint("ResourceType")
    @Override
    public void onClick(View view) {
        if (!((Button) view).getText().toString().equals("")) {
            return;
        }

        if (me1Turn) {
            ((Button) view).setText("♥");
            ((Button) view).setTextColor(Color.parseColor("#FFFF000D"));
        } else {
            ((Button) view).setText("\uD83D\uDC94");
            ((Button) view).setTextColor(Color.parseColor("#FFFFF700"));
        }




        roundCount++;

        if (checkForWin()) {
            if (me1Turn) {
                me1Wins();
            } else {
                you2Wins();
            }
        } else if (roundCount == 9) {
            draw();
        } else {
            me1Turn = !me1Turn;
        }

    }

    private boolean checkForWin() {
        String[][] field = new String[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field[i][j] = buttons[i][j].getText().toString();
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[i][0].equals(field[i][1])
                    && field[i][0].equals(field[i][2])
                    && !field[i][0].equals("")) {
                return true;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[0][i].equals(field[1][i])
                    && field[0][i].equals(field[2][i])
                    && !field[0][i].equals("")) {
                return true;
            }
        }

        if (field[0][0].equals(field[1][1])
                && field[0][0].equals(field[2][2])
                && !field[0][0].equals("")) {
            return true;
        }

        if (field[0][2].equals(field[1][1])
                && field[0][2].equals(field[2][0])
                && !field[0][2].equals("")) {
            return true;
        }

        return false;
    }

    private void me1Wins() {
        me1Points++;
        pop();
        Toast.makeText(this, "I win!", Toast.LENGTH_SHORT).show();
        updatePointsText();
        resetBoard();
    }

    private void you2Wins() {
        you2Points++;
        pop();
        Toast.makeText(this, "You win!", Toast.LENGTH_SHORT).show();
        updatePointsText();
        resetBoard();
    }

    private void draw() {
        Toast.makeText(this, "Draw!", Toast.LENGTH_SHORT).show();
        resetBoard();
    }

    private void updatePointsText() {
        textViewMe1.setText("Me : " + me1Points);
        textViewYou2.setText("You : " + you2Points);

    }

    //pop
    public void pop(){

        if(me1Points == 10){
            wiwnerdial();
        }
        else if(me1Points == 20){
            wiwnerdial();
        }else if(me1Points == 30){
            wiwnerdial();
        }else if(me1Points == 40){
            wiwnerdial();
        }else if(me1Points == 50){
            wiwnerdial();
        }else if(you2Points == 10){
            wiwnerdial2();
        }else if(you2Points == 20){
            wiwnerdial2();
        }else if(you2Points == 30){
            wiwnerdial2();
        }else if(you2Points == 40){
            wiwnerdial2();
        }else if(you2Points == 50){
            wiwnerdial2();
        }


    }


    //wiwner dialogbox
    private void wiwnerdial(){
        {
            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            if(me1Points == 10){
                builder.setMessage("Yahoo!!✧ \n My score " + me1Points);
                builder.setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
            }else if(me1Points == 20){
                builder.setMessage("I am really great!!✦ \n My score " + me1Points);
                builder.setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
            }else if(me1Points == 30){
                builder.setMessage("Ooh!!!✫ \nI am win after 20 points! \nYou are lost! \n My score " + me1Points);
                builder.setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
            }else if(me1Points == 40){
                builder.setMessage("Go away! You are really lost! \n✪! My score " + me1Points + "\n Your score " + you2Points);
                builder.setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
            }else if(me1Points == 50){
                builder.setMessage("Winning!!!❂ \nIf you are frustrations! \nthen play with me again. \nMy score " + me1Points + " \n Your score " + you2Points);
                builder.setNegativeButton("Play with me again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        resetGame();
                    }
                });
            }
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }




    //wiwner dialogbox2
    private void wiwnerdial2(){
        {
            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

            if(you2Points == 10){
                builder.setMessage("Yahoo!!✧ \n Your score " + you2Points);
                builder.setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
            }else if(you2Points == 20){
                builder.setMessage("Congrats!✦ \n You are really great!! \n Your score " + you2Points);
                builder.setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
            }else if(you2Points == 30){
                builder.setMessage("Ooh Congrats!!! \nYou win Iron medal!✫ \n You are win after 20 points! \n I am lost! \n Your score " + you2Points);
                builder.setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
            }else if(you2Points == 40){
                builder.setMessage("Go ahead! \nYou win silver medal!✪ \n Your score " + you2Points + "\n My score " + me1Points);
                builder.setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
            }else if(you2Points == 50) {
                builder.setMessage("Congratulations!!! \nYou winning the game!!! \nYou win gold medal!!!❂ \nYour score " + you2Points + " \n My score " + me1Points);


                builder.setPositiveButton("Play with me again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        resetGame();
                    }
                });


                builder.setNegativeButton("RATE US",
                        new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog,
                                                int which)
                            {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=bd.com.dod.tictactoe")); // Add package name of your application
                                startActivity(intent);
                                Toast.makeText(MainActivity.this, "Thank you for your rating.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            }

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    private void resetBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                buttons[i][j].setText("");
            }
        }

        roundCount = 0;
        me1Turn = true;
    }

    private void resetGame() {
        me1Points = 0;
        you2Points = 0;
        updatePointsText();
        resetBoard();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("roundCount", roundCount);
        outState.putInt("me1Points", me1Points);
        outState.putInt("you2Points", you2Points);
        outState.putBoolean("me1Turn", me1Turn);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        roundCount = savedInstanceState.getInt("roundCount");
        me1Points = savedInstanceState.getInt("me1Points");
        you2Points = savedInstanceState.getInt("you2Points");
        me1Turn = savedInstanceState.getBoolean("me1Turn");
    }

    //exit
    @Override
    public void onBackPressed()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Thank you !");
        builder.setMessage("Thank you for using our application. Please give us your suggestions and feedback.");
        builder.setNegativeButton("RATE US",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,
                                        int which)
                    {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=bd.com.dod.tictactoe")); // Add package name of your application
                        startActivity(intent);
                        Toast.makeText(MainActivity.this, "Thank you for your rating.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
        builder.setPositiveButton("QUIT",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,
                                        int which)
                    {
                        finish();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       getMenuInflater().inflate(R.menu.main_manu, menu);
       return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== R.id.help){
            {
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("You can play this game easly.\nFirst of all you can try touch the every box randomly\nand you can earn score and challenge it");
                builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }

        }else if (item.getItemId()== R.id.share){

            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            String shareBody = "MY SCORE IS " + you2Points + ". Play This Love game. Let's go https://play.google.com/store/apps/details?id=bd.com.dod.tictactoe";
            myIntent.putExtra(Intent.EXTRA_TEXT,shareBody);
            startActivity(Intent.createChooser(myIntent, "Share using"));


        } else{
            return super.onOptionsItemSelected(item);
        }
        return true;

    }
}

