package bd.com.dod.pdfreader;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.pdf.PdfRenderer;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private ImageView img;
    private int pagecount =0;
    private Button previous,next;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        previous = (Button)findViewById(R.id.previous);

        next = (Button)findViewById(R.id.next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pagecount++;

                render();
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pagecount--;

                render();
            }
        });


    }

    private void render() {

        try{

            img = (ImageView)findViewById(R.id.image);
            int WIDTH = img.getWidth();
            int HEIGHT = img.getHeight();

            Bitmap bitmap = Bitmap.createBitmap(WIDTH ,HEIGHT ,Bitmap.Config.ARGB_4444);

            File file = new File("sdcard/");

            PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(file , ParcelFileDescriptor.MODE_READ_ONLY));

            if(pagecount<0){
                pagecount=0;
            }else if(pagecount > renderer.getPageCount()){
                pagecount = renderer.getPageCount() -1;
            }

            Matrix m = img.getImageMatrix();
            Rect rect = new Rect(0,0,WIDTH , HEIGHT);
            renderer.openPage(pagecount).render(bitmap,rect,m, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            img.setImageMatrix(m);
            img.setImageBitmap(bitmap);
            img.invalidate();
            
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}

 tools:context="bd.com.dod.hi.MainActivity"

















<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@color/allbg"
    tools:context="bd.com.dod.hi.MainActivity"
    android:orientation="vertical">
    <ImageView
        android:id="@+id/imageView"
        android:layout_width="match_parent"
        android:layout_height="20sp"
        app:srcCompat="@drawable/bismillah"
        android:contentDescription="@string/desimg" />
    <Space
        android:layout_width="match_parent"
        android:layout_height="5dp" />
    <TextView
        android:id="@+id/textView"
        android:layout_width="match_parent"
        android:layout_height="40dp"
        android:textSize="30sp"
        android:textColor="@color/colorAccent"
        android:textStyle="bold"
        android:gravity="center"
        android:background="@drawable/swirl"
        android:text="@string/heading" />

    <Space
        android:layout_width="match_parent"
        android:layout_height="15dp" />

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical">

    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView2"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" android:contentDescription="TODO"/>
        <Button
            android:id="@+id/button"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/alif" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView3"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/ba"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/ba" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView4"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/ta"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/ta" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView5"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/sa"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/sa" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView6"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/zim"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/zim" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView7"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/ha"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/ha" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView8"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/kha"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/kha" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView9"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/dal"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/dal" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView10"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/jal"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/jal" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView11"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/ra"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/ra" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView12"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/ja"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/ja" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView13"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/sin"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/sin" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView14"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/shin"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/shin" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView15"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/soyad"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/soyad" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView16"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/doyat"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/doyat" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView17"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/toya"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/toya" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView18"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/joya"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/joya" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView19"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/ain"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/ain" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView20"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/goin"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/goin" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView21"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/fa"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/fa" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView22"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" android:contentDescription="TODO"/>
        <Button
            android:id="@+id/cof"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/cof" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView23"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/caf"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/caf" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView24"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/lam"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/lam" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView25"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/mim"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/mim" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView26"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/nun"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/nun" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView27"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/oya"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/oya" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView28"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/haha"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/haha" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView29"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/hamja"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/hamja" />
    </RelativeLayout>
    <RelativeLayout
        android:orientation="vertical"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content">
        <ImageView
            android:id="@+id/imageView30"
            android:layout_width="50sp"
            android:layout_alignParentRight="true"
            android:layout_height="50sp"
            app:srcCompat="@drawable/sound" />
        <Button
            android:id="@+id/yaa"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorbtn"
            android:textSize="20sp"
            android:textStyle="bold"
            android:text="@string/yaa" />
    </RelativeLayout>

        </LinearLayout>
    </ScrollView>



</LinearLayout>

